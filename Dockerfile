FROM alpine:3
RUN ["apk", "add", "tinyproxy"]
ADD tinyproxy.conf /etc/tinyproxy/tinyproxy.conf
ADD filter /etc/tinyproxy/filter
CMD ["tinyproxy", "-d"]
